Source: ledit
Section: editors
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>,
 Ralf Treinen <treinen@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-ocaml,
 ocaml,
 camlp5 (>= 8),
 bash-completion
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: http://cristal.inria.fr/~ddr/ledit/
Vcs-Browser: https://salsa.debian.org/ocaml-team/ledit
Vcs-Git: https://salsa.debian.org/ocaml-team/ledit.git

Package: ledit
Architecture: any
Depends:
 ${ocaml:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Provides: readline-editor
Description: line editor for interactive programs
 Ledit is a line editor, allowing to use control commands like in emacs
 or in shells (bash, tcsh). To be used with interactive commands. It is
 written in OCaml and Camlp4 and uses the library unix.cma.

Package: libledit-ocaml-dev
Architecture: any
Section: ocaml
Depends:
 ${ocaml:Depends},
 ocaml-findlib,
 camlp5,
 ${shlibs:Depends},
 ${misc:Depends}
Provides:
 ${ocaml:Provides}
Description: OCaml line editor library
 Ledit is a line editor, allowing to use control commands like in emacs
 or in shells (bash, tcsh). To be used with interactive commands. It is
 written in OCaml and Camlp4 and uses the library unix.cma.
 .
 This package ships Ledit as a development library, so that you can use
 it to build interactive programs with line editing capabilities.
